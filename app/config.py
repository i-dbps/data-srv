import os
from dotenv import load_dotenv

load_dotenv('.env')
elastic = {}
elastic['HOST'] = os.getenv('ELASTIC_HOST')
elastic['PORT'] = os.getenv('ELASTIC_PORT')

elastic['INDEX'] = os.getenv('ELASTIC_INDEX')
elastic['USER'] = os.getenv('ELASTIC_USERNAME')
elastic['PASS'] = os.getenv('ELASTIC_PASSWORD')

LOG_LEVEL  = os.getenv('APP_LOG_LEVEL')
APP_ENV = os.getenv('APP_ENV') or 'local'


KEYPOINT_NAMES = [
    'nose',
    'leftEye',
    'rightEye',
    'leftEar',
    'rightEar',
    'leftShoulder',
    'rightShoulder',
    'leftElbow',
    'rightElbow',
    'leftWrist',
    'rightWrist',
    'leftHip',
    'rightHip',
    'leftKnee',
    'rightKnee',
    'leftAnkle',
    'rightAnkle'
]

VICON_MARKER_LABELS = [
    'LFHD',
    'RFHD',
    'LBHD',
    'RBHD',
    'C7',
    'T10',
    'CLAV',
    'STRN',
    'LSHO',
    'LUPA',
    'LELB',
    'LFRM',
    'LWRA',
    'LWRB',
    'LFIN',
    'RBAK',
    'RSHO',
    'RUPA',
    'RELB',
    'RFRM',
    'RWRA',
    'RWRB',
    'RFIN',
    'LASI',
    'RASI',
    'LPSI',
    'RPSI',
    'LTHI',
    'LKNE',
    'LTIB',
    'LANK',
    'LHEE',
    'LTOE',
    'RTHI',
    'RKNE',
    'RTIB',
    'RANK',
    'RHEE',
    'RTOE'
]

MARKER_KEYPOINT_LINKS = [
[0, 1, 3],
[0, 2, 4],
[0, 1, 3],
[0, 2, 4],
[5, 6],
[5, 6, 11, 12],
[5, 6],
[5, 6, 11, 12],
[5],
[5, 7],
[7],
[7, 9],
[9],
[9],
[9],
[6],
[6],
[6, 8],
[8],
[8, 10],
[10],
[10],
[10],
[11],
[12],
[11],
[12],
[11, 13],
[13],
[13, 15],
[15],
[15],
[15],
[12, 14],
[14],
[14, 16],
[16],
[16],
[16]
]