import falcon
from falcon_multipart.middleware import MultipartMiddleware

from app.api import handlecors
from app.api import casecheck, keypoints, pearsoncorrelation

from app.errors import AppError
from app import config

from app import log
LOG = log.get_logger()


class App(falcon.API):
    def __init__(self, *args, **kwargs):
        super(App, self).__init__(*args, **kwargs)

        LOG.info('API Server is starting')
        

        self.add_route('/api/{system}/case/{case_id}/{excercise_id}/{label}', casecheck.Casecheck())
        self.add_route('/api/calc/{case_id}/{excercise_id}/{label}', pearsoncorrelation.Pearsoncorrelation())
        self.add_route('/api/items/keypoints', keypoints.Keypoints())


        self.add_error_handler(AppError, AppError.handle)


application = App(middleware=[MultipartMiddleware(),
                              handlecors.HandleCORS()])
