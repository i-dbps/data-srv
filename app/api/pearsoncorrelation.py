import json
import falcon
from elasticsearch import Elasticsearch, logger
from app import config
from app import log
from app import utils
import c3d
import datetime
import csv
import pandas as pd
import numpy as np
from scipy.stats import pearsonr
import json
import operator
import time

LOG = log.get_logger()
LOGLEVEL = 50
es_logger = logger
es_logger.setLevel(LOGLEVEL)


class Pearsoncorrelation(object):
    """generating data for forcedgraph"""
    def __init__(self):
        pd.set_option('display.max_rows', 900)
        self.formatSpecifier = '%Y-%m-%dT%H:%M:%S.%fZ'
        self.windowsize = 25
        self.rcwindowsize = 10
        self.es = Elasticsearch([{"host": config.elastic['HOST'], 
                                  "port": config.elastic['PORT']}],
                                  http_auth=(config.elastic['USER'], 
                                  config.elastic['PASS']))

    def on_get(self, req, resp, case_id, excercise_id, label):
        """get data"""

        keys = self.checkMeasures(case_id, excercise_id)
        keypointMapping = utils.namedMarkersDict(utils.mapMarkersList())

        try:
            avr = 0
            data = []
            for c3d_label in keypointMapping[label]:
              data_item = {}
              idbps_df = self.es_query(keys['es_key'], label)
              idbps_ranges = self.findOverallStartAndEndFrame(idbps_df, 3, window=7)
              c3d_df = self.c3d_query(keys['c3d_path'], c3d_label)
              c3d_ranges = self.findOverallStartAndEndFrame(c3d_df)

              c3d_ranges, idbps_ranges = self.getRanges(c3d_ranges, len(c3d_df), idbps_ranges, len(idbps_df))

              if c3d_ranges or idbps_ranges:
                c3d_df = c3d_df[c3d_ranges[0]:c3d_ranges[1]]
                c3d_df.reset_index(drop=True, inplace=True)

                idbps_df = idbps_df[idbps_ranges[0]:idbps_ranges[1]]
                idbps_df.reset_index(drop=True, inplace=True)
              
              length = min(len(c3d_df),len(idbps_df))
              c3d_df = c3d_df[:length]
              c3d_df.reset_index(drop=True, inplace=True)

              idbps_df = idbps_df[:length]
              idbps_df.reset_index(drop=True, inplace=True)


              pearson = pearsonr(c3d_df['X_SMA'], idbps_df['Y_SMA'])

              data_item['label'] = c3d_label
              data_item['corr'] = abs(pearson[0])
              data_item['p_value'] = pearson[1]

              data.append(data_item)

            resp.body = json.dumps(data)
            resp.status = falcon.HTTP_200
        except Exception as err:
            LOG.info(
                "Error calculating documents: %s", err)
            resp.body = json.dumps({})
            resp.status = falcon.HTTP_404

    def es_query(self, es_key, label):
        body = {"query": {
                    "bool": {
                        "must": [{
                            "match": {
                                "testStart": es_key
                            }
                        },
                        {
                            "match": {
                                "keypointName": label
                            }
                        }]
                    }
                },
                "sort" : [{
                    "frameDate" : {
                        "order" : "asc"
                    }
                }]
            }

        res = self.es.search(index="log*", body=json.dumps(body), size=10000)

        keypoints= []
        pointlist = []

        starttime = None
        
        for hit in res['hits']['hits']:
            if starttime == None:
                starttime = datetime.datetime.strptime(
                                hit['_source']['frameDate'], 
                                self.formatSpecifier)

            time = (datetime.datetime.strptime(
                                hit['_source']['frameDate'], 
                                self.formatSpecifier) 
                    - starttime)
            
            pointlist.append({ 'time':int(np.round((((time.seconds*1000) + (time.microseconds/1000)))/10,0)), 
                                        'x':hit['_source']['x'], 'y':hit['_source']['y']})

        data = {'name': label,
                   'list': self.addExtraPoints(pointlist)}

        idbps_df = self.movingSlope(
                      self.movingAverage(
                        self.convertListToDataframes(data), 
                        self.windowsize),
                      self.rcwindowsize)

        idbps_df.dropna(inplace=True)
        return idbps_df

    def c3d_query(self, c3d_path, label):
        reader = c3d.Reader(open(c3d_path, 'rb'))
            
        point_labels = reader.point_labels
        pointlist = {}

        for i, points, analog in reader.read_frames():
            for labelid, point in enumerate(points):
                keypointName = point_labels[labelid].strip()

                if keypointName not in pointlist.keys():
                    pointlist[keypointName] = []
                
                pointlist[keypointName].append({ 'time':i, 
                                            'x':point.tolist()[0], 
                                            'y':point.tolist()[1], 
                                            'z':point.tolist()[2]})

        hits = []
        data = {}
        for keypointName in point_labels:
            if(keypointName.strip() == label):
                data = {'name': keypointName.strip(),
                         'list': pointlist[keypointName.strip()]}


        c3d_df = self.movingSlope(
              self.movingAverage(
                self.convertListToDataframes(data), 
                self.windowsize),
              self.rcwindowsize)
        c3d_df.dropna(inplace=True)

        return c3d_df

    def checkMeasures(self, case_id, excercise_id):
        path = 'data/Testpersoon-{case_id}/'.format(case_id=case_id)
        es_key = ""

        with open('./data/metingen.csv', 'r') as file:
            csv_reader = csv.reader(file, delimiter=',')
            for row in csv_reader:
                if (row[0].upper()[-1] == case_id.upper()) and (row[2].upper() == excercise_id.upper()):
                    path = path + row[1]
                    es_key = row[5]
        return {'es_key': es_key.strip(), 'c3d_path' : path}

    def getDimensions(self, elem):
      return elem.keys()

    def convertListToDataframes(self, data):
      dim = self.getDimensions(data['list'][0])
      dfdict = {}
      for record in data['list']:
          for key in dim:
            if key != 'time':
              if key not in dfdict:
                dfdict[key] = []
              dfdict[key].append((record[key]))

      return pd.DataFrame(dfdict)

    def movingAverage(self, df, window):
      dim = df.columns.tolist()
      for i, key in enumerate(dim):
        df[key.upper() + '_SMA'] = df.iloc[:,i].rolling(window=window).mean()
      return df

    def movingSlope(self, df, window):
      dim = df.columns.tolist()
      maxrow = int(len(dim)/2)

      for i in range(0,df.shape[0]-window):
        for j in range(0, maxrow):
          df.loc[df.index[i+window],'RC_'+dim[j+maxrow].upper()] = np.round((df.iloc[i,j+maxrow] - df.iloc[i+window,j+maxrow]),0)
      return df

    def findStartAndEndFrame(self, df, column, threshold=2, window=5):
      framelist = []
      for i in range(0,df.shape[0]-window):
        if abs(df.iloc[i,column] - df.iloc[i+window,column]) > threshold:
          framelist.append(i)

      result = None
      if framelist:
        result = (min(framelist), max(framelist))
      return result


    def findOverallStartAndEndFrame(self, df, threshold=2, window=5):
      dim = df.columns.tolist()
      start = int(len(dim) - (len(dim)/3))
      end = len(dim)

      ranges = []
      for i in range(start,end):
        result = self.findStartAndEndFrame(df, i, threshold, window)
        if result:
          ranges.append(result)

      if ranges:
        max_range = max(ranges, key=operator.itemgetter(1))[1]
        min_range = min(ranges, key=operator.itemgetter(0))[0]
        if min_range == max_range:
          value = None
        else:
          value = (min_range, max_range)
      else:
        value = None
      return value

    def saveDataFrame(self, df, prefix='calc', extension='.csv'):
      filename = prefix + time.strftime("%Y%m%d-%H%M%S") + extension
      df.to_csv(filename)


    def createPoint(self, i, x1, x2, y1, y2):
      a = (y2-y1)/(x2-x1)
      b = y1
      return (a*i)+b

    def addExtraPoints(self, pointlist):
      coordinateList = []
      timer = 1
      for elem, next_elem in zip(pointlist, pointlist[1:]):
        for i in range(0, next_elem['time']-elem['time']):
          calc_x = self.createPoint(i,elem['time'], next_elem['time'], elem['x'], next_elem['x'])
          calc_y = self.createPoint(i,elem['time'], next_elem['time'], elem['y'], next_elem['y'])

          coordinateList.append({ 'time':timer, 
                                      'x':calc_x, 
                                      'y':calc_y})

          timer = timer + 1
      return coordinateList


    def getRanges(self, rangeA, max_lengthA, rangeB, max_lengthB):
        responseA = None
        responseB = None
        if rangeA and rangeB:
          length = max(rangeA[1] - rangeA[0], rangeB[1] - rangeB[0])          
          responseA = (rangeA[0], rangeA[0]+length)
          responseB = (rangeB[0], rangeB[0]+length)
        elif rangeA:
          length = rangeA[1] - rangeA[0]
          responseA = responseB = (rangeA[0], rangeA[0]+length)
        elif rangeB:
          length = rangeB[1] - rangeB[0]
          responseA = responseB = (rangeB[0], rangeB[0]+length)
        else:
          length = 0

        return (responseA, responseB)


