import json
import falcon
from app import config
from app import log
from app import utils
import c3d
import numpy as np

LOG = log.get_logger()


class Keypoints(object):
    """generating data for forcedgraph"""
    # def __init__(self):



    def on_get(self, req, resp):
        """get data"""
        # try:
        # create response data
        data = {'keypoints': utils.namedMarkersDict(utils.mapMarkersList())}

        resp.body = json.dumps(data)
        resp.status = falcon.HTTP_200
        # except :
        #     # LOG.info(
        #     #     "Error publishing documents to Elasticsearch: %s", err)
        #     resp.body = json.dumps({})
        #     resp.status = falcon.HTTP_404
