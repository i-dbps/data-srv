import os
from app import config


def getExtensionFromPath(filepath):
    return os.path.splitext(filepath)[1][1:]

def mapMarkersList():
    nlijst = [ [] for _ in range(17) ]

    for key, lijst in enumerate(config.MARKER_KEYPOINT_LINKS):
        for j in lijst:
            nlijst[j].append(key)
    return nlijst

def namedMarkersDict(itemlist):
    dlijst = {}

    for key, lijst in enumerate(itemlist):
        labellist = []
        for j in lijst:
             labellist.append(config.VICON_MARKER_LABELS[j])
        dlijst[config.KEYPOINT_NAMES[key]] = labellist
    return dlijst