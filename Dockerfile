FROM python:3.8

# Add demo app
WORKDIR /usr/src/app

# copy project
COPY . /usr/src/app/

# install dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
