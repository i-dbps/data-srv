# Python Service

Maak een clone van de repo
```
git clone https://bitbucket.org/zuydict/example-srv.git
```

Ga naar de subfolder example-srv `cd example-srv`

Doe de volgende stappen om de service in een virtual environment te starten.

## Python Virtual Environment
Eerst moeten we een python virtual environment maken.

Als je pip nog niet hebt geinstalleerd dan doe dan nu eerst, [installeer eerst](https://pip.pypa.io/en/stable/installing/).

Vervolgens installeer je virtualenv met `pip install virtualenv`.

Maak vervolgens een virtual environment om de service in te runnen:
```
python3 -m venv test_venv
source test_venv/bin/activate
pip install -r requirements.txt
```

Voor development neem je de dev variant:
```
python3 -m venv test_venv
source test_venv/bin/activate
pip install -r requirements-dev.txt
```

Voor windows zijn de stappen iets afwijkend. [Check de handleiding](https://virtualenv.pypa.io/en/stable/userguide/)

## gunicorn
Start de service met:
```
gunicorn --reload app:api
```
De service is nu gestart.

## Shutting down

Om te stoppen:
```
Ctrl+C
deactivate
```

# React Web App
Ga naar het project example-app en volg de README.md.

## Docker

Je kan het project ook met docker starten.

```
docker build -t zuyd/example-srv .
docker run -it --name falcon -v <path>:/srv -p 8000:8000 zuyd/example-srv 
```