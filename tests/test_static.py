from falcon import testing
import pytest
from rest import srv
import json


@pytest.fixture(scope='module')
def client():
    return testing.TestClient(srv.create())


def test_get_root_message(client):
    result = client.simulate_get('/')
    assert result.status_code == 404


def test_get_lock_message(client):
    body = json.dumps({'sensors': ['D', 'S', 'D'], 'access': 'Open'})

    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    result = client.simulate_post('/lock', body=body, headers=headers)
    assert result.status_code == 200


def test_get_cipher_caesar_message(client):
    result = client.simulate_get('/cipher/caesar')
    assert result.status_code == 405
